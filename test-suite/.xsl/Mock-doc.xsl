<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/mockService">
		<html>
			<head>
			</head>
			<body>
				<h1>Mocktjänst för <xsl:value-of select="id"/></h1>
				
				<h2>Om mocktjänsten</h2>
				<p>Detta dokument beskriver mocktjänsten för <xsl:value-of select="id"/>. Mock-tjänsten ger stöd i form av automatiska och manuella kontroller för att verifiera implementationen innan integrationen med den nationella tjänsteplattformen.<br/>
				Mocktjänsten använder SoapUI för att verifiera implementationen. Dokumentation om SoapUI hittas här: <a target="blank" href="https://www.soapui.org/getting-started/introduction.html">www.soapui.org</a>.<br/>
				Klicka på <a target="blank" href="https://www.soapui.org/downloads/soapui.html">den här länken</a> för att ladda hem en gratisversion av SoapUI. Installera enligt anvisning.</p>
				
				<h2>Förberedelser</h2>
				<p>
					<ul>
						<li>Gå till mappen <b>test-suite</b> i release-paketet</li>
						<li>Kopiera filen <b>‘soapui-support-N.N.NN.jar’</b> ('N.N.NN' är versionsnummer) till mappen <b>/bin/ext</b> där Soap-UI är installerat (leta efter mappen <b>/Program Files/Smartbear</b> på PC eller <b>/contents/java/app</b> på MAC OS)</li>
						<li>Öppna SoapUI och importera SoapUI-projektet (<b>test-suite/<xsl:value-of select="contractName"/>Mock/<xsl:value-of select="contractName"/>Mock-soapui-project.xml</b>) (välj ‘Import Project’ från menyn 'File')</li>
						<li>Mock-tjänsten kommer som standard att kopplas till URL <b>http://localhost:8088/mock<xsl:value-of select="contractName"/>ResponderBinding</b>. Detta kan ändras i SoapUI-projektet vid behov.</li>
						<li>Gör eventuella förändringar av inställningar i data.xml (se nedan)</li>
						<li>Starta mocktjänsten i SoapUI.</li>
						<li>Konfigurera det testade systemet så att anrop skickas till mocktjänsten.</li>
						<li>Du bör nu kunna börja använda mocktjänsten.</li>
					</ul>
				</p>
				
				<h2>Inställningar i <i>data.xml</i></h2>
				<p>
					Vissa inställningar för mocktjänsten görs i filen <i>data.xml</i>.
					Filen är i XML-format och i början finns en sektion som heter "globaldata" där generella inställningar kan göras.
					Följande parametrar finns:
					<ul>
						<xsl:for-each select="globaldata/*">
							<li>
								<b><xsl:value-of select="name()"/></b>
								<xsl:choose>
									<xsl:when test="name()='logTestData'"> - Loggning av request/response-meddelanden. Sätts till true för att aktivera. <i> Observera att patientdata kan lagras vid påslagen loggning.</i></xsl:when>
									<xsl:when test="name()='logTestDataPath'"> - Sökvägen till den katalog där loggfilerna för request/response sparas.</xsl:when>
									<xsl:when test="name()='logTestDataFilesAllowed'"> - Max antal request/response som sparas. Det blir en fil för varje anrop (äldst fil tas bort automatiskt).</xsl:when>
									<xsl:otherwise></xsl:otherwise>
								</xsl:choose>	
							</li>
						</xsl:for-each>
					</ul>
				</p>
				<p>Glöm inte att spara <i>data.xml</i> efter att du har ändrat i den!</p>

				<h2>Mock-funktionalitet</h2>
				<p>
					<!-- <xsl:value-of select="description"/> -->
					<xsl:copy-of select="description"/>
				</p>
				
				<xsl:if test="testInstructions">
					<h2>Testanvisningar</h2>
					<p>
						<xsl:copy-of select="testInstructions"/>
					</p>
				</xsl:if>
				
				<xsl:if test="testcase">
					<h2>Testfall</h2>
					<xsl:for-each select="testcase|section">
						<h3><xsl:value-of select="@id"/></h3>
						<p><xsl:copy-of select="description"/></p>
					</xsl:for-each>
				</xsl:if>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet> 